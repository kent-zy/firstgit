<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body style="text-align: center;padding-left:100px;">
	<a href="${ctx}/user/add">新增</a>
	<table border="1" >
		<thead>
			<tr>
				<td>序号</td>
				<td>ID</td>
				<td>用户名</td>
				<td>密码</td>
				<td>年龄</td>
				<td>生日</td>
				<td>修改</td>
				<td>删除</td>
			</tr>
		</thead>
		<tbody>
 			<c:if test="${!empty userlist }">
			<c:forEach var="user" items="${userlist}" varStatus="us">
				<tr>
					<td>${us.index + 1}</td>
					<td>${user.id}</td>
					<td>${user.username}</td>
					<td>${user.password}</td>
					<td>${user.age}</td>
					<td>${user.birthday}</td>
					<td><a href="${ctx}/user/edit/${user.id}">修改</a></td>
					<td><a href="${ctx}/deluser?id=${user.id}">删除</a></td>
				</tr>
			</c:forEach>
			</c:if>
		</tbody>
	</table>
</body>

</html>