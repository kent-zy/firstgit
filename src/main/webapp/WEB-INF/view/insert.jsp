<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <script type="text/javascript">  
    function addUser(){  
        var form = document.forms[0];  
        form.action = "${ctx}/addUser";  
        form.method="post";  
        form.submit();  
    }  
</script>
</head>
<body>

    <h2>添加用户</h2>
<mvc:form action="${ctx}/adduser" method="POST" modelAttribute="user">
   		用户名：<mvc:input path="username"/><br/>
		密码：<mvc:input path="password"/><br/>
		年龄：<mvc:input path="age"/><br/>
		生日：<mvc:input path="birthday"/><br/>
    <input type="submit" value="提交">
    </mvc:form >
</body>
</html>