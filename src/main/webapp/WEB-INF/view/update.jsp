<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<body>
  <h2>修改用户</h2>
	<mvc:form action="${ctx}/updateuser" method="POST" modelAttribute="user">
		<input type="hidden" name="_method" value="PUT"/>
		<mvc:hidden path="id"/>
		用户名：<mvc:input path="username"/><br/>
		密码：<mvc:input path="password"/><br/>
		年龄：<mvc:input path="age"/><br/>
		生日：<mvc:input path="birthday"/><br/>
		<input type="submit" value="submit" />
	</mvc:form>
</body>



</body>
</html>