package com.gocom.mapper;

import java.util.List;

import com.gocom.model.User;

public interface UserMapper {

	List<User> findAll();

	void delUserById(Integer id);

	void updateUser(User user);

	User selectUserById(Integer id);

	void insertUser(User user);

}
