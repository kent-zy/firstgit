package com.gocom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gocom.mapper.UserMapper;
import com.gocom.model.User;


@Service
@Transactional
public class UserService implements UserMapper {
	@Autowired
	UserMapper userMapper;

	/**
	 * 查询所有user
	 */
	public List<User> findAll() {
		List<User> all = userMapper.findAll();
		return all;
	}
/**
 * 删除用户
 */
	public void delUserById(Integer id) {
		
		userMapper.delUserById(id);
	}
/**
 * 更新用户
 */
	
	public void updateUser(User user) {
		userMapper.updateUser(user);
	}

	/**
	 * 根据id查用户
	 */
	public User selectUserById(Integer id) {
		
		User user = userMapper.selectUserById(id);
		return user;
	}
/**
 * 新增用户
 */
	public void insertUser(User user) {
		userMapper.insertUser(user);
		
	}


	
	

}
