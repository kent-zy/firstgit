package com.gocom.controller;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gocom.model.User;
import com.gocom.service.UserService;
@Controller
public class UserController {
	@Autowired
	UserService userService;
	
	@InitBinder
	public void initd( WebDataBinder binder) {
		DateFormat ft=new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(ft, true));
		
	}
	
	/**
	 * 所有用户
	 */
	@RequestMapping("/users")
	public String list(Model model) {
		List<User> list = userService.findAll();
		model.addAttribute("userlist", list);
		return "/users";
	}
	/**
	 * 跳转到新增页面
	 */
	@RequestMapping("/user/add")
		public String addPage(Model model) {
			User user = new User();
			model.addAttribute("user", user);
	        return "/insert";  
	    }

	/**
	 * 新增
	 */
	@RequestMapping("/adduser")
	public String insert(@Valid User user,Model model,Errors errors) {
		userService.insertUser(user);
		model.addAttribute("user", user); 
		return "redirect:/users";
	}
	
	/**
	 * 删除用户
	 */
	@RequestMapping("/deluser")
	public String delete( Integer id) {
		userService.delUserById(id);
		return "redirect:/users";
	}
	
	/**
	 * 更新用户
	 */
	@RequestMapping("/updateuser")
	public String update( @Valid User user,Model model,Errors errors) {
	
		userService.updateUser(user);
        model.addAttribute("user", user);  
      
		return "redirect:/users";
	}
	/**
	 * 查询用户
	 */

	@GetMapping("/user/edit/{id}")
	public String editPage(@PathVariable Integer id, Model model) {
		User user = userService.selectUserById(id);
		model.addAttribute("user", user);
		return "/update";
	}


}
