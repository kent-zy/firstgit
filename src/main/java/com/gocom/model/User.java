package com.gocom.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
@Data
public class User {
	
	private Integer id;
	@NotBlank
	private String username;
	@NotBlank
	private String password;
	 @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date birthday;
	 @Range(min=0,max=99)
	private Integer age;

}
