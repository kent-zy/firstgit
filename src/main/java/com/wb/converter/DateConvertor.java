package com.wb.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class DateConvertor implements Converter<String, Date>{

	public Date convert(String source) {
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
			
					Date date;
					try {
						date = format.parse(source);
						return date;
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
		
		return null;	
	}
	

}
